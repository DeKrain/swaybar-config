from collections import namedtuple
import selectors

def read_file(path, binary=True):
    with open(path, 'rb' if binary else 'r') as f:
        return f.read()

EventEntry = namedtuple('EventEntry', ('target', 'fd', 'ev'))

class EventWatch:
    def __init__(self, fos, ev_mask):
        self.sel = selectors.DefaultSelector()
        for fo in fos:
            self.sel.register(fo, ev_mask)
    def fileno(self):
        return self.sel.fileno()
    def poll(self, timeout=None):
        evs = self.sel.select(timeout)
        for idx in range(len(evs)):
            key, ev = evs[idx]
            evs[idx] = EventEntry(key.fileobj, key.fd, ev)
        return evs
