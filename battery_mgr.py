import os
import time
from util import read_file

def task_battery(block):
    bl_now, bl_life = block.subdivide(2)
    bl_now.prevent_auto_push()
    bl_life.prevent_auto_push()
    power_supplies = '/sys/class/power_supply'
    battery = power_supplies + '/BAT1'
    design = int(read_file(battery + '/charge_full_design'))
    """
    ch_max = fwatch(battery + '/charge_full', signalled = True)
    ch_now = fwatch(battery + '/charge_now', signalled = True)
    watch = EventWatch([ch_max, ch_now], selectors.EVENT_READ)
    while True:
        evs = watch.poll()
        for tev in evs:
            if ev.target is ch_now:
                bl_now.set_text(f'{int(ch_now.read()) / int(ch_max.read()) * 100.0:.2}%')
            elif ev.target is ch_full:
                bl_life.set_text(f'{int(ch_max.read()) / design * 100.0:.2}%')
    """
    fd_max = os.open(battery + '/charge_full', os.O_RDONLY)
    fd_now = os.open(battery + '/charge_now', os.O_RDONLY)
    last_life = None
    last_now = None
    urgent_level = 0.20
    while True:
        bat_max = os.read(fd_max, 0x400)
        bat_now = os.read(fd_now, 0x400)
        os.lseek(fd_max, 0, os.SEEK_SET)
        os.lseek(fd_now, 0, os.SEEK_SET)

        bat_max = int(bat_max)
        bat_now = int(bat_now)
        r_life = f'{bat_max / design * 100.0:.2f}%'
        r_now = f'{bat_now / bat_max * 100.0:.2f}%'
        changed = False
        if r_life != last_life:
            last_life = r_life
            bl_life.set_text('LIFE: ' + r_life)
            changed = True
        if r_now != last_now:
            last_now = r_now
            bl_now.set_urgent(bat_now / bat_max < urgent_level)
            bl_now.set_text('BAT: ' + r_now)
            changed = True
        if changed:
            block.ipc.render()
        time.sleep(5)
