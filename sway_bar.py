#!/bin/env python3

import json
import sys
import os
import time
from threading import Thread

with open('/proc/self/comm', 'w') as f:
    f.write('sway-status-bar')

comm = sys.stdout

json.dump({
    'version': 1,
    'click_events': False,
}, comm)
print(file=comm)
print('[', file=comm)

def send_ipc(obj: list['Block']):
    json.dump(obj, comm)
    print(',')
    comm.flush()


class BlockProxy:
    def __init__(self, ipc, index: int):
        self.ipc = ipc
        self.index = index
        self.text = None
        self.subblocks = None
        self._auto_push = True
        self._urgent = False
    def set_text(self, text: str):
        self.text = text
        #msg = [{}] * self.index
        #msg.append({
        #    'full_text': text,
        #})
        #send_ipc(msg)
        if self._auto_push:
            self.ipc.render()
    def subdivide(self, nr) -> list['BlockProxy']:
        assert self.subblocks is None
        self.subblocks = [BlockProxy(ipc, (self.index, idx)) for idx in range(nr)]
        return self.subblocks
    def prevent_auto_push(self):
        self._auto_push = False
    def set_urgent(self, urgent=True):
        self._urgent = urgent

class IpcProxy:
    def __init__(self):
        self.blocks = []
        self._ipc_ready = False

    def setup_complete(self):
        self._ipc_ready = True

    def render(self):
        if not self._ipc_ready:
            return
        msgs = []
        stack = [iter(self.blocks)]
        while len(stack) != 0:
            top = stack.pop()
            for proxy in top:
                if proxy.subblocks is not None:
                    stack.append(top)
                    stack.append(iter(proxy.subblocks))
                    break
                if proxy.text is None:
                    continue
                bl = {
                    'full_text': proxy.text,
                }
                if proxy._urgent:
                    bl['urgent'] = True
                msgs.append(bl)
        send_ipc(msgs)

ipc = IpcProxy()

def launch_task(callback):
    proxy = BlockProxy(ipc, len(ipc.blocks))
    ipc.blocks.append(proxy)
    proxy.thread = Thread(name=f'task-{callback.__name__}', target=callback, args=(proxy,))
    proxy.thread.start()

# For testing
def counter(block):
    n = 0
    while True:
        n += 1
        block.set_text(str(n))
        time.sleep(0.7)

def task_time(block):
    while True:
        rep = time.strftime('%Y-%m-%d %H:%M:%S')
        block.set_text(rep)
        time.sleep(1)


from battery_mgr import task_battery
import wp_volume

launch_task(task_battery)
launch_task(lambda block: wp_volume.App().main(block))
# Time at the end
launch_task(task_time)
ipc.setup_complete()
