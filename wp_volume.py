"""
WirePlumber volume monitor.
Based on snippets from WirePlumber/wpctl tool (MIT licensed):
  Copyright © 2019-2020 Collabora Ltd.
  @author George Kiagiadakis <george.kiagiadakis@collabora.com>
"""

import gi
import time

gi.require_version('Wp', '0.5')
from gi.repository import GObject, GLib, Wp

plugins_to_load = (
    'libwireplumber-module-default-nodes-api',
    'libwireplumber-module-mixer-api',
)

def helper_new_object_interest(type, constraints):
    interest = Wp.ObjectInterest.new_type(type)
    for con in constraints:
        interest.add_constraint(*con)
    return interest

class App:
    def main(self, block):
        # Setup
        self.block = block
        Wp.init(Wp.InitFlags.ALL)

        self.ml = GLib.MainLoop()
        self.core = Wp.Core()
        self.om = Wp.ObjectManager()

        # Prepare
        self.om.add_interest_full(Wp.ObjectInterest.new_type(Wp.Node))
        self.om.request_object_features(Wp.GlobalProxy, Wp.ProxyFeatures.PIPEWIRE_OBJECT_FEATURES_MINIMAL)

        self.plugins_to_load = len(plugins_to_load)
        for plugin in plugins_to_load:
            self.core.load_component(plugin, 'module', None, None, None, self.on_plugin_loaded)

        self.core.connect()

        GObject.Object.connect(self.core, 'disconnected', self.ml.quit)
        self.om.connect('installed', self.run)

        self.ml.run()

    def on_plugin_loaded(self, core, result):
        self.plugins_to_load -= 1
        core.load_component_finish(result)
        if self.plugins_to_load == 0:
            mixer_api = Wp.Plugin.find(self.core, 'mixer-api')
            mixer_api.set_property('scale', 1) # 1 = Cubic
            self.core.install_object_manager(self.om)

    def run(self, _om):
        self.def_nodes_api = Wp.Plugin.find(self.core, 'default-nodes-api')
        self.mixer_api = Wp.Plugin.find(self.core, 'mixer-api')

        self.default_sink()

        self.def_nodes_api.connect('changed', self.default_changed)
        self.mixer_api.connect('changed', self.params_changed)

    def default_changed(self, _def_node_api):
        self.default_sink()

    def default_sink(self):
        self.sink = self.def_nodes_api.emit('get-default-node', 'Audio/Sink')
        if self.sink == 0 or self.sink == 0xFFFF_FFFF:
            raise ValueError('No default sink')

        proxy = self.om.lookup_full(helper_new_object_interest(Wp.GlobalProxy, [
            (Wp.ConstraintType.G_PROPERTY,
                'bound-id', Wp.ConstraintVerb.EQUALS, GLib.Variant.new_uint32(self.sink)),
        ]))
        if proxy is None:
            raise ValueError('Node not found')
        self.print_params()

    def params_changed(self, _mixer_api, node_id):
        if self.sink != node_id:
            return
        self.print_params()

    def print_params(self):
        variant = self.mixer_api.emit('get-volume', self.sink)
        if variant is None:
            raise ValueError('Node doesn\'t support volume')
        volume = variant.lookup_value('volume').get_double()
        muted = variant.lookup_value('mute').get_boolean()
        prefix = 'Vol: 🔇' if muted else 'Vol: '
        self.block.set_text(f'{prefix}{volume:.2f}')
